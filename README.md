**Logysto-Front**

Frontend desarrollado con angular para visualizar las apis desarrolladas.

**PRE-REQUISITOS**

1. Instalación del cli de Angular


2. Instalación de node y npm


**INSTALACIÓN Y PUESTA EN MARCHA**

1. Clonar el repositorio `git clone https://gitlab.com/adrianvergara22/logysto-maps-front.git`


2. Ejecutar el comando `npm install`


3. Si en el backend modificó el puerto por el cual se levanta el servidor de node, debe modificar la uri del back en el fichero `environments/environment.ts`


4. Ejecutar el comando `ng serve` para visualizar el front en modo desarrollo


5. Si desea compilar el front para producción debe ejecutar el comando `npm run build --prod` y visualizar en el navegador el index.html que se genera dentro del directorio `dist`
