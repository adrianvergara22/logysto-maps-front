const base = 'http://localhost:3200/api';

export const environment = {
  production: false,
  serverName: base,
};
