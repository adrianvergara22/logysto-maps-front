export enum status  {
  PENDING = 'pendiente',
  PENDING_PREPARATION = 'pendiente preparacion',
  PREPARING = 'preparando',
  READY = 'listo',
  PENDING_DELIVERY = 'pendiente entrega',
  ON_WAY = 'en camino',
  DELIVERED = 'entregado',
  RECEIVED = 'recibido',
  CANCEL = 'cancelado'
}


export enum status_pay {
  PAY = 'pagado',
  PENDING = 'pendiente',
  FAILED = 'rechazado',
}
