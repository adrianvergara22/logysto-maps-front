import {HttpCacheService} from './config/httpcache.service';
import {Injectable} from '@angular/core';
import {ServersService} from './config/server.service';

@Injectable()
export class GeolocationService {
  constructor(private http: HttpCacheService, private servers: ServersService) {
  }


  searchLocation(address: string, callback: any, callbackError: any) {
    return this.http.get(0, `${this.servers.serverName}/v1/geolocations/address/${address}`, callback, callbackError);
  }

}
