import {Injectable} from '@angular/core';
import {SnackBarComponent} from '../../components/common/snack-bar/snack-bar.component';
import Swal from 'sweetalert2';

@Injectable()
export class SnackBarService {
  constructor(private snackbar: SnackBarComponent) {

  }

  success(message: string) {
    this.snackbar.openSnackBar(message, 'Ok', 'snackBar__success');
  }

  error(message: string) {
    this.snackbar.openSnackBar(message, 'Error', 'snackBar__error');
  }

  swalSuccess(message: string) {
    Swal.fire('¡Enhorabuena!', message, 'success');
  }

  swalError(message: string) {
    Swal.fire('Lo sentimos...', message, 'error');
  }

  swalConfirm(title: string, text: string, confirmButtonText = '¡Si, Eliminar!'): Promise<any> {
    return Swal.fire({
      title,
      text,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#008B8A',
      cancelButtonText: 'Cancelar',
      confirmButtonText
    });
  }

}
