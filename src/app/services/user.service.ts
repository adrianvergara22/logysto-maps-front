import {HttpCacheService} from './config/httpcache.service';
import {Injectable} from '@angular/core';
import {ServersService} from './config/server.service';
import {CanActivate, Router} from '@angular/router';

@Injectable()
export class UserService implements CanActivate {

  constructor(private http: HttpCacheService, private servers: ServersService, private router: Router) {
  }

  create(data: any, callback: any, callbackError: any) {
    return this.http.post(0, `${this.servers.serverName}/v1/users`, data, callback, callbackError);
  }

  canActivate() {
    if (this.isExist()) {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }

  get() {
    let me = {};
    if (sessionStorage.getItem('me') && sessionStorage.getItem('token')) {
      me = JSON.parse(sessionStorage.getItem('me'));
    }
    return me;
  }

  getIdUser() {
    let me = '';
    if (sessionStorage.getItem('me') && sessionStorage.getItem('token')) {
      me = JSON.parse(sessionStorage.getItem('me')).id;
    }
    return me;
  }

  isExist() {
    let exist = false;
    if (sessionStorage.getItem('me') && sessionStorage.getItem('token')) {
      exist = true;
    }
    return exist;
  }

  token() {
    let token = '';
    if (sessionStorage.getItem('me') && sessionStorage.getItem('token')) {
      token = JSON.parse(sessionStorage.getItem('token'));
    }
    return token;
  }

  logout() {
    sessionStorage.removeItem('me');
    sessionStorage.removeItem('token');
  }


}
