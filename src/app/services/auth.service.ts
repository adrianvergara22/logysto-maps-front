import {HttpCacheService} from './config/httpcache.service';
import {Injectable} from '@angular/core';
import {ServersService} from './config/server.service';

@Injectable()
export class AuthService {
  constructor(private http: HttpCacheService, private servers: ServersService) {
  }


  authenticate(data: any, callback: any, callbackError: any) {
    return this.http.post(0, `${this.servers.serverName}/v1/auth`, data, callback, callbackError);
  }


}
