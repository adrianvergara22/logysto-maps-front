import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable()
export class ServersService {
  constructor() { }
  serverName: string = environment.serverName;

  getLocale() {
    return JSON.parse(localStorage.getItem('auth_item'));
  }
}

export class Result {
  success: boolean;
  message: string;
  content: any;
}
