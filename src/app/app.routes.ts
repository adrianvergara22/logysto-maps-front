import {Routes} from '@angular/router';
import {BlankComponent} from './components/common/layouts/blank.component';
import {BasicComponent} from './components/common/layouts/basic.component';
import {AuthComponent} from './views/auth/auth.component';
import {UserService} from './services/user.service';
import {GeolocationComponent} from './views/geolocation/geolocation.component';

export const ROUTES: Routes = [

  // Main redirect
  {path: '', redirectTo: 'login', pathMatch: 'full'},

  // App views
  {
    path: '', component: BasicComponent,
    children: [
      {
        path: 'geolocations',
        component: GeolocationComponent,
        canActivate: [UserService]
      }
    ]
  },

  // Login template
  {
    path: '', component: BlankComponent,
    children: [
      {path: 'login', component: AuthComponent},
    ]
  },

  // Handle all other routes
  {path: '**', component: GeolocationComponent}
];
