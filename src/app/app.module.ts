import {ListEmptyComponent} from './components/common/list-empty/list-empty.component';
import {environment} from './../environments/environment';

import {MaterialModule} from './modules/material.module';
import {MatNativeDateModule} from '@angular/material/core';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

import {HttpConfigInterceptor} from './config/httpconfig.interceptor';
import {HTTP_INTERCEPTORS} from '@angular/common/http';

import {ServicesModule} from './modules/services.module';
import {RouterModule} from '@angular/router';
import {ROUTES} from './app.routes';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


// App modules/components
import {LayoutsModule} from './components/common/layouts/layouts.module';
import {AuthComponent} from './views/auth/auth.component';
import {SnackBarComponent} from './components/common/snack-bar/snack-bar.component';

// sockets
import {SocketIoModule, SocketIoConfig} from 'ngx-socket-io';


import {registerLocaleData} from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import {IconsModule} from './modules/icons.module';
import {GeolocationComponent} from './views/geolocation/geolocation.component';
import {AgmCoreModule} from '@agm/core';
import { RegisterUserComponent } from './views/auth/register-user/register-user.component';

registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    SnackBarComponent,
    ListEmptyComponent,
    GeolocationComponent,
    RegisterUserComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatNativeDateModule,
    IconsModule,

    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDaOkSnYIzRp-xbUGI4Tu83l5tySyVcW2Y',
    }),

    // Modules
    LayoutsModule,
    ServicesModule,
    RouterModule.forRoot(ROUTES),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true
    },
    SnackBarComponent

  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
