import {
  ServersService,
  HttpCacheService,
  AuthService,
  SnackBarService
} from './../services/config/base.import';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {UserService} from '../services/user.service';
import {GeolocationService} from '../services/geolocation.service';

@NgModule({
  imports: [HttpClientModule],
  providers: [
    ServersService,
    HttpCacheService,
    AuthService,
    SnackBarService,
    UserService,
    GeolocationService
  ]
})
export class ServicesModule {
}
