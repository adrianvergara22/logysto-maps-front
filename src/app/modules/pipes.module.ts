
import { NgModule } from '@angular/core';
import {FilterPipe} from '../pipes/filter.pipe';
import {MycurrencyPipe} from '../pipes/currency.pipe';
import { OrderBy } from '../pipes/orderBy.pipe';
@NgModule({
  declarations: [FilterPipe,MycurrencyPipe,OrderBy],
  exports: [FilterPipe,MycurrencyPipe,OrderBy]
})
export class PipesModule {}
