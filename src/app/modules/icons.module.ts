import {NgModule} from '@angular/core';

import {FeatherModule} from 'angular-feather';
import {
  Info,
  Menu,
  List,
  ShoppingCart,
  ChevronLeft,
  ChevronRight,
  MapPin,
  Watch,
  Phone,
  Plus,
  X,
  Check,
  Home,
  LogIn,
  Award,
  Box,
  User,
  Lock,
  LogOut,
  ShoppingBag,
  Package,
  Video,
  Users,
  Globe
} from 'angular-feather/icons';

// Select some icons (use an object, not an array)
const icons = {
  Menu,
  ShoppingCart,
  ChevronLeft,
  ChevronRight,
  MapPin,
  Watch,
  Phone,
  Plus,
  X,
  Check,
  Home,
  LogIn,
  Award,
  Box,
  User,
  Lock,
  LogOut,
  ShoppingBag,
  Package,
  List,
  Info,
  Video,
  Users,
  Globe
};

@NgModule({
  imports: [
    FeatherModule.pick(icons)
  ],
  exports: [
    FeatherModule
  ]
})
export class IconsModule {
}
