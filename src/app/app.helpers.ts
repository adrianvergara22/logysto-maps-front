declare var jQuery: any;
import Swal from 'sweetalert2';

export function showLoading(text) {


  document.getElementById('loading').style.display = 'block';
  document.getElementById('text').innerHTML = text;

}

export function hideLoading() {

  document.getElementById('loading').style.display = 'none';
  document.getElementById('text').innerHTML = '';

}

export function isEmpty(data) {
  if (data !== undefined && data !== null && data !== '') {
    return false;
  }
  return true;
}

export const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  showClass: {
    popup: 'animate__animated animate__fadeInDown'
  },
  hideClass: {
    popup: 'animate__animated animate__fadeOutUp'
  },
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer);
    toast.addEventListener('mouseleave', Swal.resumeTimer);
  }
});

