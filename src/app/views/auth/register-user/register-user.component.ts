import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {hideLoading, showLoading} from '../../../app.helpers';
import {Result} from '../../../services/config/server.service';
import {UserService} from '../../../services/user.service';
import {SnackBarService} from '../../../services/snackbar/snackbar.service';
import {Router} from '@angular/router';

interface IUser {
  name: string;
  lastName: string;
  phone: string;
  address: string;
  email: string;
  password: string;
}

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {

  user: IUser;

  constructor(
    public dialogRef: MatDialogRef<RegisterUserComponent>,
    private userService: UserService,
    private snackBarService: SnackBarService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.user = {name: '', lastName: '', phone: '', address: '', email: '', password: ''};
  }

  close(): void {
    this.dialogRef.close();
  }

  create(): void {
    showLoading('Registrando usuario...');
    this.userService.create(this.user, (response: Result) => {
      const user = response.content.user;
      const auth = response.content.auth;
      sessionStorage.setItem('token', auth.accessToken);
      sessionStorage.setItem('refreshToken', auth.refreshToken);
      sessionStorage.setItem('me', JSON.stringify(user));
      this.snackBarService.swalSuccess(response.message);
      this.close();
      this.router.navigateByUrl('/geolocations');
      hideLoading();
    }, (response: Result) => {
      hideLoading();
      this.snackBarService.swalError(response.message);
    });
  }

}
