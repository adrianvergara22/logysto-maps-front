import {UserService} from './../../services/user.service';
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService, SnackBarService} from '../../services/config/base.import';
import {Result} from '../../services/config/server.service';
import {hideLoading, showLoading, isEmpty} from '../../app.helpers';
import {MatDialog} from '@angular/material/dialog';
import {RegisterUserComponent} from './register-user/register-user.component';

interface IAuth {
  email: string;
  password: string;
}

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  credentials: IAuth;

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private authService: AuthService,
    private snackBarService: SnackBarService,
    private user: UserService) {
  }

  ngOnInit(): void {
    this.user.logout();
    this.credentials = {email: '', password: ''};
  }

  auth(): void {
    showLoading('Iniciando Sesión');
    this.authService.authenticate(this.credentials, (response: Result) => {
      const user = response.content.user;
      const auth = response.content.auth;
      sessionStorage.setItem('token', auth.accessToken);
      sessionStorage.setItem('refreshToken', auth.refreshToken);
      sessionStorage.setItem('me', JSON.stringify(user));
      this.snackBarService.swalSuccess(response.message);
      this.router.navigateByUrl('/geolocations');
      hideLoading();
    }, (response: Result) => {
      hideLoading();
      this.snackBarService.swalError(response.message);
    });
  }

  openRegisterModal() {
    const dialogRef = this.dialog.open(RegisterUserComponent, {
      width: '45%',
    });
  }

}
