import {Component, OnInit} from '@angular/core';
import {hideLoading, showLoading} from '../../app.helpers';
import {GeolocationService} from '../../services/geolocation.service';
import {Result} from '../../services/config/server.service';
import {SnackBarService} from '../../services/snackbar/snackbar.service';

@Component({
  selector: 'app-geolocation',
  templateUrl: './geolocation.component.html',
  styleUrls: ['./geolocation.component.css']
})
export class GeolocationComponent implements OnInit {

  address: any = {
    address: '',
    provider: '',
    location: {latitude: '', longitude: ''}
  };
  configMap: any = {
    center: {
      latitude: 5.134651501930159, longitude: -73.56101389418342
    },
    zoom: 6
  };

  constructor(
    private geolocationService: GeolocationService,
    private snackBarService: SnackBarService,
  ) {
  }

  ngOnInit(): void {
  }

  dragEnd(event: any) {
    console.log(event);
    this.address.location.latitude = Number(event.coords.lat);
    this.address.location.longitude = Number(event.coords.lng);
    console.log('Direccion Arrastrada');
    console.log(this.address.location);
  }

  searchLocation() {
    showLoading('Buscando la ubicación.');
    this.geolocationService.searchLocation(this.address.address, (response: Result) => {
      const {latitude, longitude, provider} = response.content;
      this.configMap = {center: {latitude, longitude}, zoom: 18};
      this.address.location = {latitude, longitude};
      this.address.provider = provider;
      hideLoading();
      console.log(response);
    }, (response: Result) => {
      this.snackBarService.swalError(response.message);
      hideLoading();
    });
  }

}
