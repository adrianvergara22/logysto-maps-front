import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-list-empty',
  templateUrl: './list-empty.component.html',
  styleUrls: ['./list-empty.component.css']
})
export class ListEmptyComponent implements OnInit {

  constructor() {
  }

  @Input() message: string;
  @Input() icon: string;

  ngOnInit(): void {
  }

}
