import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-snackbar',
  templateUrl: './snack-bar.component.html',
  styleUrls: ['./snack-bar.component.css']
})

export class SnackBarComponent implements OnInit {

  timeOut = 1500;

  constructor(
    public snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {

  }


  openSnackBar(message, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: this.timeOut,
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      panelClass: [className],
    });

  }


}