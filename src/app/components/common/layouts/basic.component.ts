import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../../services/user.service';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'basic',
  templateUrl: 'basic.template.html',
  styleUrls: ['./basic.component.css']

})
export class BasicComponent implements OnInit, OnDestroy {

  userId: string;
  events: string[] = [];
  opened = true;
  title: string;
  user: any = {};
  menus: any = [];

  constructor(
    private userService: UserService,
    private router: Router
  ) {
    this.user = this.userService.get();
    this.userId = this.userService.getIdUser();

    this.menus = [
      {routerLink: '/geolocations', activeRoute: 'Geolocations', iconName: 'package', title: 'Geolocalización'},
    ];
  }

  ngOnInit(): void {
  }


  logout() {
    this.userService.logout();
    this.router.navigateByUrl('login');
  }

  activeRoute(routename: string): boolean {
    return this.router.url.indexOf(routename) > -1;
  }


  ngOnDestroy() {
    console.log('Exit basic component :)');
  }

  logOut(): void {
    sessionStorage.clear();
    this.router.navigateByUrl('auth/login');
  }

}
