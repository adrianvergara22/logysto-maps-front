import { LoadingComponent } from './../loading/loading.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MaterialModule } from './../../../modules/material.module';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BlankComponent } from './blank.component';
import { BasicComponent } from './basic.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {IconsModule} from '../../../modules/icons.module';
@NgModule({
    declarations: [BlankComponent, BasicComponent, LoadingComponent],
    imports: [BrowserModule, RouterModule, MaterialModule, FormsModule, ReactiveFormsModule, IconsModule],
    exports: [BlankComponent, BasicComponent, LoadingComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class LayoutsModule { }
